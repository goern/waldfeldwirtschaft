# waldfeldwirtschaft

This is v0.2 of waldfeldwirtschaft, a simple forest management game. Right now we can declare our own world, in a
file called `worldv2.ttl`, and run a simulation on it. The simulation is very simple. Use `./main.py` to run it.
The output will be something like:

```bash
Process: http://example.org/world#TreeGrowth
Inputs:
  - Input URI: http://example.org/world#Soil, Quantity: 100
  - Input URI: http://example.org/world#Water, Quantity: 100
Output: http://example.org/world#Tree
Output Quantity: 1
Production Time: P3D

Process: http://example.org/world#WoodChopping
Inputs:
  - Input URI: http://example.org/world#Tree, Quantity: 15
Output: http://example.org/world#Log
Output Quantity: 6
Production Time: PT4H

Process: http://example.org/world#CraftPlank
Inputs:
  - Input URI: http://example.org/world#Log, Quantity: 1
Output: http://example.org/world#Plank
Output Quantity: 4
Production Time: PT5H

Current simulation time: PT0H0M0S
http://example.org/world#Soil: 2000 items
http://example.org/world#Water: 2000 items
http://example.org/world#Tree: 0 items
http://example.org/world#Plank: 0 items
http://example.org/world#Log: 0 items
Running simulation...
Current simulation time: PT8760H0M0S
http://example.org/world#Soil: 0 items
http://example.org/world#Water: 0 items
http://example.org/world#Tree: 5 items
http://example.org/world#Plank: 24 items
http://example.org/world#Log: 0 items
```

It shows that the simulation ran for 8760 hours (1 year) and that we have 5 trees and 24 planks at the end of it. All
of the soil and water has been used by the TreeGrowth process.

## Running the simulation

`poetry run python main.py`
