#!/usr/bin/env python3
#
# see my conversation at https://chat.openai.com/share/ba1a2050-fff6-4b9a-bbc1-7925a304d106
# for more details
#

import logging

import simpy
from rdflib import Graph

from waldfeldwirtschaft.processes import ProductionProcess
from waldfeldwirtschaft.sparql import INVENTORY, PROCESS_MECHANICS
from waldfeldwirtschaft.utils import (
    convert_to_seconds,
    print_natural_resources,
    show_inventory,
)

logging.basicConfig(level=logging.INFO)

# Initialization of the SimPy environment
env = simpy.Environment()


# Load the RDF file
g = Graph()
g.parse("world.ttl", format="turtle")


# Perform the SPARQL query on the Graph to get all the process mechanics
results = g.query(PROCESS_MECHANICS)

processes = {}

# Process the results
for row in results:
    process_uri = str(row.process)

    if process_uri not in processes:
        processes[process_uri] = {
            "inputs": [],
            "outputs": str(row.output),
            "input_quantities": [],
            "output_quantity": str(row.outputQuantity),
            "production_time": str(row.productionTime),
        }

    input_good = str(row.inputGood)
    input_qty = str(row.inputQuantity)
    processes[process_uri]["inputs"].append(input_good)
    processes[process_uri]["input_quantities"].append(input_qty)
    processes[process_uri]["name"] = str(row.name)


print_natural_resources(g)


# Perform the SPARQL query on the Graph to get the initial inventory
init_results = g.query(INVENTORY)

inventory = {}
for row in init_results:
    good = str(row.good)
    initial_quantity = int(row.initialQuantity)
    inventory[good] = simpy.Container(env, init=initial_quantity, capacity=5000)


# For each production process, create an instance of the ProductionProcess class
for process, details in processes.items():
    inputs = details["inputs"]
    input_quantities = [int(qty) for qty in details["input_quantities"]]
    output = details["outputs"]
    output_quantity = int(details["output_quantity"])
    production_time = convert_to_seconds(details["production_time"])

    ProductionProcess(
        env,
        process,
        details["name"],
        inputs,
        input_quantities,
        output,
        output_quantity,
        production_time,
        inventory,
    )
    logging.debug(f"This is Process {process}: {details}")

# Show initial inventory
show_inventory(env, inventory)

# Run the simulation
print("Running simulation...")
# env.process(write_inventory(env, inventory, 24 * 7))
env.run(until=convert_to_seconds("PT8760H0M0S"))

# Show final inventory
show_inventory(env, inventory)
