"""
This module contains the global variables for the supplychain ontology.
"""

from rdflib import URIRef

SUPPLYCHAIN_NAMESPACE = URIRef("http://example.com/ontology/supplychain/")
