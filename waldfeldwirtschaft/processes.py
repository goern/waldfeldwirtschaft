import logging

import simpy

from waldfeldwirtschaft.natural_resource import NaturalResource


class ProductionProcess:
    def __init__(
        self,
        env: simpy.Environment,
        process,
        name: str,
        inputs,
        input_quantities: int,
        output,
        output_quantity: int,
        production_time: int,
        inventory,
    ):
        logging.debug(f"adding ProductionProcess for '{name}'")
        self.env = env
        self.process = process
        self.name = name
        self.inputs = inputs
        self.input_quantities = input_quantities
        self.output = output
        self.output_quantity = output_quantity
        self.production_time = production_time
        self.inventory = inventory
        self.action = env.process(self.run())  # start the process when an instance is created

    def run(self):
        # while True:
        for t in range(self.production_time):
            # Try to get the required input quantities from the inventory
            for i, input_good in enumerate(self.inputs):
                logging.debug(f"input_good is of type {type(input_good)}, called {input_good}")
                required_quantity = self.input_quantities[i]
                if isinstance(input_good, NaturalResource):
                    logging.debug(f"Skipping {input_good} as it is a natural resource")
                else:
                    logging.debug(f"Trying to get {required_quantity} of {input_good}")
                    try:
                        yield self.inventory[input_good].get(required_quantity)
                        logging.debug(f"Got {required_quantity} of {input_good}")
                    except KeyError:
                        logging.error(f"Could not find {input_good} in the inventory. Please check your input data.")
                        raise ValueError(f"Could not find {input_good} in the inventory.")

            # Wait for the production time
            logging.debug(f"Waiting for production time: {self.production_time} for process {self.process}")
            # yield self.env.timeout(self.production_time)
            yield self.env.timeout(1)

            # Put the output quantity to the inventory
            logging.debug(f"Trying to put {self.output_quantity} of {self.output}")
            try:
                yield self.inventory[self.output].put(self.output_quantity)
            except KeyError:
                logging.info(f"Could not find {self.output} in the inventory. Adding it.")
                self.inventory[self.output] = simpy.Container(self.env, init=self.output_quantity, capacity=5000)
            logging.debug(f"Put {self.output_quantity} of {self.output}")
