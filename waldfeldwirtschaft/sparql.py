# Query for initial quantities
INVENTORY = """
    PREFIX sc: <http://example.com/ontology/supplychain/>
    PREFIX : <http://example.org/world#>
    SELECT ?good ?initialQuantity WHERE {
        ?good rdf:type sc:Good .
        ?good sc:hasInitialQuantity ?initialQuantity .
    }
"""


# Define a SPARQL query to retrieve all production processes and their details
PROCESS_MECHANICS = """
PREFIX sc: <http://example.com/ontology/supplychain/>
PREFIX : <http://example.org/world#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT ?process ?name ?input ?inputGood ?inputQuantity ?output ?outputQuantity ?productionTime WHERE {
    ?process rdf:type sc:ProductionProcess .
    ?process rdf:label ?name .
    ?process sc:hasInput ?input .
    ?input sc:hasGood ?inputGood .
    ?input sc:hasQuantity ?inputQuantity .
    ?process sc:hasOutput ?output .
    ?process sc:hasOutputQuantity ?outputQuantity .
    ?process sc:hasProductionTime ?productionTime .
}
"""

# Define a SPARQL query to retrieve all natural resources
ALL_NATURAL_RESOURCE = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT ?resource
WHERE {
  ?resource rdf:type/rdfs:subClassOf* sc:NaturalResource .
}
"""
