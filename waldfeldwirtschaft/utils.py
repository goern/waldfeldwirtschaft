import csv
import datetime
from typing import Any

import isodate
from rdflib import Graph
from simpy import Environment

from waldfeldwirtschaft.sparql import ALL_NATURAL_RESOURCE


def convert_to_seconds(iso_duration) -> int:
    """Convert an ISO 8601 duration string to seconds."""
    duration = isodate.parse_duration(iso_duration)
    return int(duration.total_seconds())


def seconds_to_iso(seconds) -> Any | str:
    """Convert seconds to an ISO 8601 duration string."""
    iso_duration = isodate.duration_isoformat(datetime.timedelta(seconds=seconds))
    return iso_duration


def show_inventory(env: Environment, inventory: dict) -> None:
    """Show the current inventory of all goods."""
    current_time = env.now  # get the current simulation time
    iso_time = seconds_to_iso(current_time)  # convert to ISO 8601 duration string

    print(f"Current simulation time: {iso_time}")
    for good, container in inventory.items():
        print(f"{good}: {container.level} items")


def write_inventory(env, inventory, interval):
    """Write the current inventory to a CSV file every interval time units."""
    while True:
        # Wait for the next interval
        yield env.timeout(interval)

        # Open the CSV file for writing
        with open("inventory.csv", "a", newline="") as file:
            writer = csv.writer(file)

            # Write the inventory
            for good, store in inventory.items():
                writer.writerow([env.now, good, store.level])


def print_natural_resources(g: Graph):
    """Print all instances of the NaturalResource class."""
    results = g.query(ALL_NATURAL_RESOURCE)

    print("Natural Resources:")
    for row in results:
        print(f"- {row.resource}")
